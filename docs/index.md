## Foreword

Hello! This is an example student blog for the [2023-2024 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/class-website/).

## About me

![](images/avatar-photo.jpg)

Bonjour, je m'appelle Jacob et je suis étudiant en BA 2 Biologie à l'ULB

Visit this website to see my work!

## My background

I was born in a nice city called..

## Previous work

I'm a paragraph. Edit the page on Gitlab to add your own text and edit me.  I’m a great place for you to tell a story and let your users know a little more about you.​

### Project A

This is an image from an external site:

![This is the image caption](https://images.unsplash.com/photo-1512436991641-6745cdb1723f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ad25f4eb5444edddb0c5fb252a7f1dce&auto=format&fit=crop&w=900&q=80)

While this is an image from the assets/images folder. Never use absolute paths (starting with /) when linking local images, always relative.

![This is another caption](images/sample-photo.jpg)
